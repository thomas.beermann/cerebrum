FROM adoptopenjdk:11-jre-hotspot
COPY target/helmholtz-cerebrum-*.jar cerebrum.jar
EXPOSE 8090
CMD java -Dspring.profiles.active=dev -Dspring.data.mongodb.authentication-database=${MONGODB_AUTH_DB} -Dspring.data.mongodb.database=${MONGODB_DB} -Dspring.data.mongodb.host=${MONGODB_HOST} -Dspring.data.mongodb.port=${MONGODB_PORT} -Dspring.data.mongodb.username=${MONGODB_USERNAME} -Dspring.data.mongodb.password=${MONGODB_PASSWORD}  -Dsecurity.oauth2.client.pre-established-redirect-uri=${OAUTH2_REGISTERED_URI} -Dsecurity.oauth2.client.registered-redirect-uri=${OAUTH2_REGISTERED_URI} -Dsecurity.oauth2.client.use-current-uri=${OAUTH2_USE_CURRENT_URI} -jar cerebrum.jar
