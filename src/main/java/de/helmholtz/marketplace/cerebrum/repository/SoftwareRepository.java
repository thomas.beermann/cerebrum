package de.helmholtz.marketplace.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.marketplace.cerebrum.entity.Software;
import de.helmholtz.marketplace.cerebrum.repository.fragment.CerebrumRepository;

public interface SoftwareRepository extends MongoRepository<Software, String>, CerebrumRepository<Software>
{
    Page<Software> findByName(String name, PageRequest page);
    Optional<Software> findByUuid(String uuid);
    Optional<Software> deleteByUuid(String uuid);
}
