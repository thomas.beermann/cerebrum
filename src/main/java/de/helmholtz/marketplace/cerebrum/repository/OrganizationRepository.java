package de.helmholtz.marketplace.cerebrum.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.marketplace.cerebrum.entity.Organization;
import de.helmholtz.marketplace.cerebrum.repository.fragment.CerebrumRepository;

public interface OrganizationRepository extends MongoRepository<Organization, String>, CerebrumRepository<Organization>
{
    Optional<Organization> findByUuid(String uuid);

    @SuppressWarnings("UnusedReturnValue")
    Optional<Organization> deleteByUuid(String id);
}
