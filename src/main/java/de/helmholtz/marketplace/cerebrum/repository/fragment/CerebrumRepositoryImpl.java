package de.helmholtz.marketplace.cerebrum.repository.fragment;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Set;

import de.helmholtz.marketplace.cerebrum.utils.CerebrumEntityUuidGenerator;

public class CerebrumRepositoryImpl<T> implements CerebrumRepository<T>
{
    private final MongoOperations mongoTemplate;
    private static final String KEY = "foreignKeys";

    public CerebrumRepositoryImpl(MongoOperations mongoTemplate)
    {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void addForeignKey(String uuid, String foreignKey)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(uuid));

        Update update = new Update();
        update.addToSet(KEY, foreignKey);
        mongoTemplate.updateFirst(query, update, CerebrumEntityUuidGenerator.getClass(uuid));
    }

    @Override
    public void removeForeignKey(String uuid, String foreignKey)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(uuid));

        Update update = new Update();
        update.pull(KEY, foreignKey);
        mongoTemplate.updateFirst(query, update, CerebrumEntityUuidGenerator.getClass(uuid));
    }

    @Override
    public T updateForeignKeys(String uuid, Set<String> keys)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(uuid));

        Update update = new Update();
        update.set(KEY, keys);
        mongoTemplate.updateFirst(query, update, CerebrumEntityUuidGenerator.getClass(uuid));
        return null;
    }
}
